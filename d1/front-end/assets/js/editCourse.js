let formSubmit2 = document.querySelector('#editCourse');




formSubmit2.addEventListener('submit', (e) => {
	e.preventDefault()


	let courseName = document.querySelector('#courseName').value
	let description = document.querySelector('#courseDescription').value
	let price = document.querySelector('#coursePrice').value

	let token = localStorage.getItem('token');
	
	const urlParams = new URLSearchParams(window.location.search);
    const courseId = urlParams.get('courseId');
    

	 fetch('http://localhost:4000/api/courses', { 
	 method: 'PUT',
      headers: {
	  'Content-Type': 'application/json',
	   'Authorization': `Bearer ${token}`

	},
	  	body: JSON.stringify({
        courseId: courseId,	
   		name: courseName,
   		description: description,
   		price: price
  
   })

})
	  
	  .then(res => {
   	  return res.json()
   })
   .then(data => {
   	//creation of new course successful
   	if(data === true){
   		//redirect to course page

   		alert('Updated!')
   		window.location.replace("./course.html")
   	}else{
   		//redirect in creating course
   		alert('Something went wrong')
   	 }

    })

})
